﻿using System.Drawing;
using OpenTK;
using Tri;
using Tri.And;
using Tri.And.GameLoop;
using Tri.And.Rendering.Camera;
using Tri.And.UI;
using Tri.And.UI.BitmapFont;

namespace MipmappingTest
{
    public class TestGameLoop : GameLoopBase
    {
        OrthographicCamera ocam;
        BitmapLabel texparam, lb1, lb2, lb3, lb4;

        public TestGameLoop()
            : base("_TestGameLoop")
        {
            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport / 2);
        }

        protected override void OnInitialize()
        {
            BitmapFont arial = BitmapFont.Create("SystemResources/arial.fnt");
            string lorem =
@"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

            texparam = new BitmapLabel()
            {
                Position = new Vector2(0, GameSystem.MainForm.Viewport.Y),
                Anchor = new Vector2(0, 1),
                Color = Color.Black,
                FontSize = 30,
                BitmapFont = arial,
                Text = "LinearMipmapLinear",
            };
            lb1 = new BitmapLabel()
            {
                Anchor = new Vector2(0, 1),
                Color = Color.Black,
                FontSize = 10,
                BitmapFont = arial,
                Text = lorem,
            };
            lb2 = new BitmapLabel()
            {
                Anchor = new Vector2(0, 1),
                Color = Color.Black,
                FontSize = 20,
                BitmapFont = arial,
                Text = lorem,
            };
            lb3 = new BitmapLabel()
            {
                Anchor = new Vector2(0, 1),
                Color = Color.Black,
                FontSize = 30,
                BitmapFont = BitmapFont.Create("SystemResources/arial.fnt"),
                Text = lorem,
            };
            lb4 = new BitmapLabel()
            {
                Anchor = new Vector2(0, 1),
                Color = Color.Black,
                FontSize = 40,
                BitmapFont = arial,
                Text = lorem,
            };
        }

        public override void Update(Second deltaTime)
        {
            lb1.Position = new Vector2(0, texparam.Position.Y - texparam.Size.Y);
            lb2.Position = new Vector2(0, lb1.Position.Y - lb1.Size.Y);
            lb3.Position = new Vector2(0, lb2.Position.Y - lb2.Size.Y);
            lb4.Position = new Vector2(0, lb3.Position.Y - lb3.Size.Y);
        }

        public override void Render(Second deltaTime)
        {
            texparam.Render(ref ocam.Matrix);
            lb1.Render(ref ocam.Matrix);
            lb2.Render(ref ocam.Matrix);
            lb3.Render(ref ocam.Matrix);
            lb4.Render(ref ocam.Matrix);
        }
    }
}

