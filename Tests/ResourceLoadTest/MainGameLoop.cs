﻿using System;
using OpenTK;
using Tri;
using Tri.And;
using Tri.And.GameLoop;
using Tri.And.Rendering;
using Tri.And.Rendering.Camera;
using Tri.And.UI;

namespace ResourceLoadTest
{
    public class MainGameLoop : GameLoopBase
    {
        OrthographicCamera ocam;
        Sprite sp;

        public MainGameLoop()
            : base("_MainGameLoop")
        {   
        }
        protected override void OnInitialize()
        {
            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport / 2);
            sp = new Sprite()
            {
                Position = GameSystem.MainForm.Viewport / 2,
                Size = new Vector2(100, 100),
                Texture = Texture.CreateFromBitmapPath("brushed_steel.png"),
            };
        }

        public override void Update(Second deltaTime)
        {
        }
        public override void Render(Second deltaTime)
        {
            sp.Render(ref ocam.Matrix);
        }
    }
}
