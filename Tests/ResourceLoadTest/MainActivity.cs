﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Tri.And;

namespace ResourceLoadTest
{
    // the ConfigurationChanges flags set here keep the EGL context
    // from being destroyed whenever the device is rotated or the
    // keyboard is shown (highly recommended for all GL apps)
    [Activity(Label = "ResourceLoadTest",
                ConfigurationChanges = ConfigChanges.KeyboardHidden,
                ScreenOrientation = ScreenOrientation.SensorLandscape,
                MainLauncher = true,
                Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Remove title/status bar
            Window.AddFlags(WindowManagerFlags.Fullscreen);
            RequestWindowFeature(WindowFeatures.NoTitle);

            GameSystem.Initialize(this);
            GameSystem.RunGameLoop(new ResourceLoadLoop());

            GameSystem.IsDebug = true;
            GameSystem.DoUnitTest = true;

            SetContentView(GameSystem.MainForm);
        }

        protected override void OnPause()
        {
            // never forget to do this!
            base.OnPause();
        }

        protected override void OnResume()
        {
            // never forget to do this!
            base.OnResume();
        }
    }
}

