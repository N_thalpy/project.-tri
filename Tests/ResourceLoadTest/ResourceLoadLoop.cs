﻿using System;
using System.IO;
using Android.Graphics;
using Tri;
using Tri.And;
using Tri.And.GameLoop;
using Tri.And.Rendering;

namespace ResourceLoadTest
{
    public class ResourceLoadLoop : GameLoopBase
    {
        public bool Loaded;

        public ResourceLoadLoop()
            : base("_ResourceLoadView")
        {
            Loaded = false;
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            GameSystem.LogSystem.WriteLine("Begin");
            System.Threading.Tasks.Task.Run(() => 
            {
                try
                {
                    Texture.CreateFromBitmapPath("brushed_steel.png");
                }
                catch (Exception)
                {
                    GameSystem.LogSystem.WriteLine("Crashed");
                }
                finally
                {
                    GameSystem.LogSystem.WriteLine("End");
                }

                Loaded = true;
            });
        }

        public override void Render(Second deltaTime)
        {
        }

        public override void Update(Second deltaTime)
        {
            if (Loaded == true)
                GameSystem.RunGameLoop(new MainGameLoop());
        }
    }
}
