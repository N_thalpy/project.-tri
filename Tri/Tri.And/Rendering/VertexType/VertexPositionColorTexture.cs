﻿using System;
using System.Runtime.InteropServices;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES20;
using Tri.And.Rendering.Shader;

namespace Tri.And.Rendering.VertexType
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VertexPositionColorTexture
    {
        public Vector3 Position;
        public Color4 Color;
        public Vector2 TextureUV;

        public static void Bind(VertexPositionColorTexture[] vertices, ShaderBase shader, Texture tex, ref Matrix4 transform, int vertCount)
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            unsafe
            {          
                fixed (VertexPositionColorTexture* pVPCT = vertices)
                {
                    int stride = BlittableValueType<VertexPositionColorTexture>.Stride;
                    IntPtr pv = new IntPtr(pVPCT);

                    GL.UseProgram(shader.Program);

                    GLHelper.CheckGLError();
                    GL.BindAttribLocation(shader.Program, 0, "vPos");
                    GL.EnableVertexAttribArray(GL.GetAttribLocation(shader.Program, "vPos"));
                    GL.VertexAttribPointer(GL.GetAttribLocation(shader.Program, "vPos"), 3, VertexAttribPointerType.Float, false, stride, pv);

                    GLHelper.CheckGLError();
                    GL.BindAttribLocation(shader.Program, 0, "vColor");
                    GL.EnableVertexAttribArray(GL.GetAttribLocation(shader.Program, "vColor"));
                    GL.VertexAttribPointer(GL.GetAttribLocation(shader.Program, "vColor"), 4, VertexAttribPointerType.Float, false, stride, pv + sizeof(Vector3));

                    GLHelper.CheckGLError();
                    GL.BindAttribLocation(shader.Program, 0, "vCoord");
                    GL.EnableVertexAttribArray(GL.GetAttribLocation(shader.Program, "vCoord"));
                    GL.VertexAttribPointer(GL.GetAttribLocation(shader.Program, "vCoord"), 2, VertexAttribPointerType.Float, false, stride, pv + sizeof(Vector3) + sizeof(Color4));

                    GLHelper.CheckGLError();
                    GL.BindTexture(TextureTarget.Texture2D, tex.TextureHandle);

                    GLHelper.CheckGLError();
                    GL.UniformMatrix4(GL.GetUniformLocation(shader.Program, "trans"), false, ref transform);

                    GLHelper.CheckGLError();
                    GL.DrawArrays(BeginMode.Triangles, 0, vertCount);
                    GL.DisableVertexAttribArray(GL.GetAttribLocation(shader.Program, "vPos"));
                    GL.DisableVertexAttribArray(GL.GetAttribLocation(shader.Program, "vCoord"));

                    GLHelper.CheckGLError();
                }
            }

            GL.Disable(EnableCap.Blend);
        }
    }
}
