﻿using OpenTK;
using OpenTK.Graphics;
using System.Drawing;

namespace Tri.And.Rendering.Renderer
{
    public struct RenderParameter
    {
        public Vector3 Position;
        public Vector2 Anchor;
        public Vector2 Size;
        public Degree Angle;
        public Color4 Color;
        public RectangleF TextureUV;
    }
}
