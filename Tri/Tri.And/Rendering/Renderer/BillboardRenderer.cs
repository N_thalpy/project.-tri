﻿using System.Diagnostics;
using OpenTK;
using Tri.And.Rendering.Shader;
using Tri.And.Rendering.VertexType;

namespace Tri.And.Rendering.Renderer
{
    public sealed class BillboardRenderer : RendererBase
    {
        private const int VertexPerQuad = 6;

        public int MaxQuad
        {
            get;
            private set;
        }

        private VertexPositionColorTexture[] vertices;
        private int vPos;

        private SimpleShader shader;

        // caching
        private VertexPositionColorTexture vertex;

        #region .ctor
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="maxQuad">Count of quad</param>
        public BillboardRenderer(int maxQuad)
            : base()
        {
            this.MaxQuad = maxQuad;

            vertices = new VertexPositionColorTexture[maxQuad * VertexPerQuad];
        }
        #endregion

        public void Begin()
        {
            if (shader == null)
                shader = new SimpleShader();

            Debug.Assert(state == RendererState.Idle);
            state = RendererState.Drawing;

            vPos = 0;
        }
        public override void Render(RenderParameter param)
        {
            Debug.Assert(state == RendererState.Drawing);
            
            Vector2 tl = new Vector2(-param.Anchor.X * param.Size.X, (1 - param.Anchor.Y) * param.Size.Y);
            Vector2 tr = new Vector2((1 - param.Anchor.X) * param.Size.X, (1 - param.Anchor.Y) * param.Size.Y);
            Vector2 bl = new Vector2(-param.Anchor.X * param.Size.X, -param.Anchor.Y * param.Size.Y);
            Vector2 br = new Vector2((1 - param.Anchor.X) * param.Size.X, -param.Anchor.Y * param.Size.Y);

            vertex.Color = param.Color;

            // 0 -> 2 -> 1 -> 2 -> 1 -> 3

            vertex.TextureUV = param.TextureUV.GetTopLeft();
            vertex.Position = param.Position + new Vector3(tl.Rotate(param.Angle));
            vertices[vPos + 0] = vertex;

            vertex.TextureUV = param.TextureUV.GetTopRight();
            vertex.Position = param.Position + new Vector3(tr.Rotate(param.Angle));
            vertices[vPos + 2] = vertex;
            vertices[vPos + 4] = vertex;

            vertex.TextureUV = param.TextureUV.GetBottomLeft();
            vertex.Position = param.Position + new Vector3(bl.Rotate(param.Angle));
            vertices[vPos + 1] = vertex;
            vertices[vPos + 3] = vertex;

            vertex.TextureUV = param.TextureUV.GetBottomRight();
            vertex.Position = param.Position + new Vector3(br.Rotate(param.Angle));
            vertices[vPos + 5] = vertex;

            vPos += 6;
        }
        public void End(ref Matrix4 transform, Texture texture)
        {
            Debug.Assert(state == RendererState.Drawing);
            Debug.Assert(vPos % VertexPerQuad == 0);
            Debug.Assert(vPos / VertexPerQuad <= MaxQuad);

            GLHelper.CheckGLError();
            VertexPositionColorTexture.Bind(vertices, shader, texture, ref transform, vPos);
            GLHelper.CheckGLError();

            state = RendererState.Idle;
            GLHelper.CheckGLError();
        }
    }
}
