﻿using System.Diagnostics;
using OpenTK.Graphics.ES20;

namespace Tri.And.Rendering
{
    public static class GLHelper
    {
        [Conditional("DEBUG")]
        public static void CheckGLError()
        {
            var error = GL.GetErrorCode();
            if (error != ErrorCode.NoError)
                System.Console.WriteLine(error);
            Debug.Assert(error == ErrorCode.NoError);
        }
    }
}
