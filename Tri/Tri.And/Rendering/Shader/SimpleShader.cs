﻿using System;

namespace Tri.And.Rendering.Shader
{
    public class SimpleShader : ShaderBase
    {
        private static readonly String vSource = @"
#version 100

uniform mat4 trans;

attribute vec3 vPos;
attribute vec2 vCoord;
attribute vec4 vColor;

varying mediump vec4 vary_color;
varying mediump vec2 vary_coord;

void main ()
{
    vary_color = vColor;
    vary_coord = vCoord;

    gl_Position = trans * vec4(vPos, 1.0);
}";
        private static readonly String fSource = @"
#version 100

uniform sampler2D tex;

varying mediump vec4 vary_color;
varying mediump vec2 vary_coord;

void main ()
{
    gl_FragColor = vary_color * texture2D(tex, vary_coord);
}";

        public SimpleShader()
            : base(SimpleShader.vSource, SimpleShader.fSource)
        {
        }
    }
}
