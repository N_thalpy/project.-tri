﻿using OpenTK;
using OpenTK.Graphics;
using Tri.And.Rendering;
using Tri.And.Rendering.Renderer;
using Tri.And.SystemComponent;
using System.Drawing;

namespace Tri.And.UI
{
    public class Sprite : View
    {
        public Color Color;
        public Texture Texture;
		public RectangleF UV;

        private BillboardRenderer br;

        /// <summary>
        /// .ctor
        /// </summary>
        public Sprite()
            : base()
        {
            Color = Color.White;
            br = new BillboardRenderer(1);
            UV = new RectangleF(0, 0, 1, 1);
        }

        /// <summary>
        /// Draws sprite
        /// </summary>
        protected override void OnRender(ref Matrix4 mat)
        {
            if (Size == Vector2.Zero)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Size of sprite is (0, 0)");
            if (Texture == null)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Texture of sprite is null");
            else if (Texture.TextureHandle == 0)
                GameSystem.LogSystem.WriteLine(LogType.Error, "Texture is disposed");
            
            br.Begin();
            br.Render(new RenderParameter()
            {
                Position = new Vector3(Position.X, Position.Y, -Depth),
                Anchor = Anchor,
                Size = Size,
                Angle = Angle,
                Color = Color,
                TextureUV = UV,
            });
            br.End(ref mat, Texture);
        }
    }
}
