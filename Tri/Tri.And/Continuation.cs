﻿using System;
using System.Diagnostics;

namespace Tri.And
{
    public class Continuation
    {
        private Func<Object> Func;
        public Object Result
        {
            get;
            private set;
        }
        public bool Finished
        {
            get;
            private set;
        }

        public Continuation(Func<Object> a)
        {
            Func = a;
            Finished = false;
        }
        public void Invoke()
        {
            Debug.Assert(Finished == false);

            Result = Func.Invoke();
            Finished = true;
        }
    }
}
