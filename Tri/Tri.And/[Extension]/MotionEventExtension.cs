﻿using OpenTK;
using System;
using Android.Views;

namespace Tri.And
{
    public static class MotionEventExtension
    {
        public static Vector2 GetXY(this MotionEvent me)
        {
            return new Vector2(me.GetX(), me.GetY());
        }
        public static Vector2 GetXY(this MotionEvent me, int pointerIndex)
        {
            return new Vector2(me.GetX(pointerIndex), me.GetY(pointerIndex));
        }

        public static MotionEventActions Action(this MotionEvent me, int pointerIndex)
        {
            MotionEventActions down = MotionEventActions.Down;
            MotionEventActions up = MotionEventActions.Up;
            MotionEventActions move = MotionEventActions.Move;

            switch (me.Action)
            {
                case MotionEventActions.Down:
                    return down;
                case MotionEventActions.Pointer1Down:
                    return pointerIndex == 0 ? down : move;
                case MotionEventActions.Pointer2Down:
                    return pointerIndex == 1 ? down : move;
                case MotionEventActions.Pointer3Down:
                    return pointerIndex == 2 ? down : move;

                case MotionEventActions.Up:
                    return up;
                case MotionEventActions.Pointer1Up:
                    return pointerIndex == 0 ? up : move;
                case MotionEventActions.Pointer2Up:
                    return pointerIndex == 1 ? up : move;
                case MotionEventActions.Pointer3Up:
                    return pointerIndex == 2 ? up : move;

                case MotionEventActions.Move:
                    return move;
                    
                default:
                    throw new NotImplementedException(me.Action.ToString());
            }
        }
    }
}

