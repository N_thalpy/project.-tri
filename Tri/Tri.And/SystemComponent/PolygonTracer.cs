﻿using OpenTK;
using System;
using System.Collections.Generic;
using Tri.Physics;
using Tri.And.Rendering;
using Tri.And.UI;

namespace Tri.And.SystemComponent
{
    public class PolygonTracer
    {
        /// <summary>
        /// Dictionary of tracing polygons
        /// </summary>
        private Dictionary<Polygon, List<Sprite>> tracerDic;

        /// <summary>
        /// Texture of tracer
        /// </summary>
        private Texture tracerTexture;

        /// <summary>
        /// .ctor
        /// </summary>
        public PolygonTracer()
        {
            tracerDic = new Dictionary<Polygon, List<Sprite>>();
        }

        public void Initialize()
        {
            tracerTexture = Texture.CreateFromBitmapPath("SystemResources/PolygonTracer.png");
        }

        /// <summary>
        /// Start to trace polygon
        /// </summary>
        /// <param name="polygon"></param>
        [Obsolete("PolygonTracer will not work properly")]
        public void Trace(Polygon polygon)
        {
            tracerDic.Add(polygon, new List<Sprite>());
        }

        /// <summary>
        /// Update tracers
        /// </summary>
        public void Update()
        {
            foreach (Polygon polygon in tracerDic.Keys)
            {
                tracerDic[polygon].Clear();
                foreach (Vector2 v2 in polygon.PointList)
                {
                    tracerDic[polygon].Add(new Sprite()
                    {
                        Position = v2,
                        Size = new Vector2(5, 5),
                        Depth = 0,
                        Texture = tracerTexture,
                    });
                }
            }
        }
        /// <summary>
        /// Render tracers
        /// </summary>
        public void Render()
        {
            if (GameSystem.IsDebug)
                foreach (List<Sprite> spList in tracerDic.Values)
                    foreach (Sprite sp in spList)
                        sp.Render(ref GameSystem.DebugCamera.Matrix);
        }
    }
}
