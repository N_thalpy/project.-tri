﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using OpenTK;

namespace Tri.And.SystemComponent
{
    /// <summary>
    /// OpenTK input helper/wrapper
    /// </summary>
    public static class InputHelper
    {
        public class TouchData
        {
            public int Index;
            public Vector2 Position;
            public MotionEventActions Action;

            public override string ToString()
            {
                return string.Format("Idx{0}, Pos: {1}, Type {2}", Index, Position, Action);
            }
        }

        public static List<TouchData> TouchList;

        private static Queue<MotionEvent> eventQueue;

        /// <summary>
        /// static .ctor
        /// </summary>
        static InputHelper()
        {
            TouchList = new List<TouchData>();
            eventQueue = new Queue<MotionEvent>();
        }

        public static void SetMainContext(View view)
        {
            view.Touch += OnTouch;
        }

        public static void Update()
        {
            TouchList = TouchList.Where(_ => _.Action != MotionEventActions.Up).ToList();
            TouchList.Where(_ => _.Action == MotionEventActions.Down).ToList()
                .ForEach((_) =>
                {
                    _.Action = MotionEventActions.Move;
                });
            
            if (eventQueue.Count != 0)
                TouchList.Clear();
            
            while (eventQueue.Count != 0)
            {    
                var me = eventQueue.Dequeue();
                
                for (int idx = 0; idx < me.PointerCount; idx++)
                {
                    TouchData td;
                    if (TouchList.Exists(_ => _.Index == idx))
                        td = TouchList.Find(_ => _.Index == idx);
                    else
                    {
                        td = new TouchData();
                        td.Index = idx;
                        TouchList.Add(td);
                    }

                    td.Position = me.GetXY(idx);
                    td.Position = new Vector2(td.Position.X, GameSystem.MainForm.Viewport.Y - td.Position.Y);
                    switch (me.Action(idx))
                    {
                        case MotionEventActions.Move:
                            td.Action = MotionEventActions.Move;        
                            break;

                        case MotionEventActions.Down:
                            td.Action = MotionEventActions.Down;
                            break;

                        case MotionEventActions.Up:
                            td.Action = MotionEventActions.Up;
                            foreach (TouchData e in TouchList)
                                if (e.Index > td.Index)
                                    e.Index = td.Index - 1;
                            
                            td.Index = -1;
                            break;

                        default:
                            throw new ArgumentException();
                    }
                }
            }
        }

        /// <summary>
        /// Raises the touch event.
        /// can raised more than two time in one frame.
        /// </summary>
        /// <param name="e">E.</param>
        /// <param name="args">Arguments.</param>
        private static void OnTouch(object e, View.TouchEventArgs args)
        {
            eventQueue.Enqueue(args.Event);
        }
    }
}
