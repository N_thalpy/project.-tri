﻿using Tri.And.UI;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Linq;
using Tri.And.Rendering;
using Tri.And.UI.BitmapFont;
using System.Collections.Generic;

namespace Tri.And.SystemComponent
{
    public class Profiler : IDisposable
    {
        /// <summary>
        /// TextView to display profiled data
        /// </summary>
        private BitmapLabel label;

        /// <summary>
        /// Last deltaTime of update/render tick
        /// </summary>
        private float lastUpdateTime;
        private float lastRenderTime;

        private Stopwatch updateStopwatch;
        private Stopwatch renderStopwatch;

        /// <summary>
        /// Max recommended memory usage.
        /// SHOULD check memory leakage or wrong optimization when memory usage is over this value.
        /// </summary>
        private const long WarningMemoryUsage = 1000 * 1000 * 1000;

        /// <summary>
        /// StringBuilder (for caching)
        /// </summary>
        private StringBuilder sb;

        /// <summary>
        /// .ctor
        /// </summary>
        public Profiler()
        {
            sb = new StringBuilder();

            updateStopwatch = new Stopwatch();
            renderStopwatch = new Stopwatch();

            updateStopwatch.Start();
            renderStopwatch.Start();
        }

        public void Initialize()
        {
            label = new BitmapLabel()
            {
                Position = new Vector2(0, 0),
                Anchor = new Vector2(0, 0),
                Depth = 0,
                BitmapFont = BitmapFont.Create("SystemResources/arial.fnt"),
                Color = Color4.White,
            };
        }
        
        /// <summary>
        /// Get frame per second
        /// </summary>
        /// <returns>fps</returns>
        public float GetFPS()
        {
            if (lastUpdateTime <= 0f)
                return float.NaN;
            else
                return 1 / (lastUpdateTime + lastRenderTime);
        }
        /// <summary>
        /// Get current memory usage
        /// </summary>
        /// <returns>current memory usage</returns>
        public long GetMemoryUsage()
        {
            long memuse = Process.GetCurrentProcess().PrivateMemorySize64;
            if (memuse > WarningMemoryUsage)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Memory Usage is {0} now. Should check memory optimization", memuse);

            return memuse;
        }

        /// <summary>
        /// Returns profiling message
        /// </summary>
        /// <returns></returns>
        public String GetProfileMessage()
        {
            sb.Clear();
            sb.AppendLine(String.Format("Tri Library {0}", GameSystem.GameVersion));
            sb.AppendLine(String.Format("FPS: {0:F1}", GetFPS()));
            sb.AppendLine(String.Format("Frame Interval: {0:F2}ms (Render {1:F2}ms, Update {2:F2}ms)", 
                1000 * (lastUpdateTime + lastRenderTime), 
                1000 * lastRenderTime, 1000 * lastUpdateTime));
            sb.AppendLine(String.Format("Memory: {0}MB", GetMemoryUsage() / (1000 * 1000)));

            return sb.ToString().Trim();
        }

        public void UpdateStart()
        {
            updateStopwatch.Start();
        }
        public void UpdateEnd()
        {
            lastUpdateTime = (float)updateStopwatch.ElapsedTicks / Stopwatch.Frequency;
            updateStopwatch.Stop();
            updateStopwatch.Reset();
        }
        public void RenderStart()
        {
            renderStopwatch.Start();
        }
        public void RenderEnd()
        {
            lastRenderTime = (float)renderStopwatch.ElapsedTicks / Stopwatch.Frequency;
            renderStopwatch.Stop();
            renderStopwatch.Reset();
        }

        /// <summary>
        /// Render profiler
        /// </summary>
        public void Render()
        {
            if (GameSystem.IsDebug)
            {
                label.Text = GetProfileMessage();
                label.Render(ref GameSystem.DebugCamera.Matrix);
            }
        }

        /// <summary>
        /// Dispose Profiler
        /// </summary>
        public void Dispose()
        {
            label.Dispose();
        }
    }
}
