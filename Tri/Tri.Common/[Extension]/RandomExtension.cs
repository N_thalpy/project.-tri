﻿using System;

namespace Tri
{
    public static class RandomExtension
    {
        #region Degree
        /// <summary>
        /// Return Degree-typed variable in [0, 360)
        /// </summary>
        /// <param name="rd"></param>
        /// <returns></returns>
        public static Degree NextDegree(this Random rd)
        {
            return rd.NextDegree(Degree.Zero, (Degree)360);
        }
        /// <summary>
        /// Return Degree-typed variable in [min, max)
        /// </summary>
        /// <param name="rd"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Degree NextDegree(this Random rd, Degree min, Degree max)
        {
            return (Degree)rd.NextFloat((float)min, (float)max);
        }
        #endregion
        #region Second
        /// <summary>
        /// Return Second-typed variable in [min, max)
        /// </summary>
        /// <param name="rd"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static Second NextSecond(this Random rd, Second min, Second max)
        {
            return (Second)rd.NextFloat(min, max);
        }
        #endregion

        #region Float
        /// <summary>
        /// Return float-typed variable in [0, 1)
        /// </summary>
        public static float NextFloat(this Random rd)
        {
            return rd.NextFloat(0, 1);
        }
        public static float NextFloat(this Random rd, float min, float max)
        {
            return (float)rd.NextDouble() * (max - min) + min;
        }
        #endregion
    }
}
