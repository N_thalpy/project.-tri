﻿using NLua;
using System;

namespace Tri
{
    public static class LuaFunctionExtension
    {
        public static T Call<T>(this LuaFunction ftn, params Object[] args)
        {
            Object[] result = ftn.Call(args);
            if (result.Length != 1)
                throw new ArgumentException(String.Format("Return value of function is multi-valued; {0} values", result.Length));

            return CastHelper.Cast<T>(result[0]);
        }
        public static Tuple<T1, T2> Call<T1, T2>(this LuaFunction ftn, params Object[] args)
        {
            Object[] result = ftn.Call(args);
            if (result.Length != 2)
                throw new ArgumentException(String.Format("Return value of function is not 2-valued; {0} values", result.Length));

            return new Tuple<T1, T2>(CastHelper.Cast<T1>(result[0]), CastHelper.Cast<T2>(result[1]));
        }
    }
}
