﻿using NLua;
using System;

namespace Tri
{
    public static class LuaExtension
    {
        public static T DoString<T>(this Lua state, String str)
        {
            Object[] result = state.DoString(str);
            if (result == null || result.Length != 1)
                throw new ArgumentException(String.Format("Return value of function is multi-valued; {0} values", result.Length));

            return CastHelper.Cast<T>(result[0]);
        }
        public static Tuple<T1, T2> DoString<T1, T2>(this Lua state, String str)
        {
            Object[] result = state.DoString(str);
            if (result == null || result.Length != 2)
                throw new ArgumentException();

            return new Tuple<T1, T2>(CastHelper.Cast<T1>(result[0]), CastHelper.Cast<T2>(result[1]));
        }
    }
}

