﻿using System.Diagnostics;

namespace Tri
{
    public static class StopwatchExtension
    {
        public static Second GetElapsedSecond(this Stopwatch sw)
        {
            return (Second)(sw.ElapsedMilliseconds / 1000f);
        }
    }
}

