﻿using System;
using System.Diagnostics;
using Tri.SystemComponent;

namespace Tri
{
    [Serializable]
    [UnitTest]
    public struct Degree
    {
        /// <summary>
        /// inner value.
        /// </summary>
        public float InnerValue;

        #region static
        /// <summary>
        /// zero
        /// </summary>
        public static Degree Zero
        {
            get
            {
                return (Degree)0f;
            }
        }
        #endregion

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="value"></param>
        public Degree(float value)
        {
            InnerValue = value;
        }

        #region Type Casting
        public static explicit operator Degree(float f)
        {
            return new Degree(f);
        }
        public static explicit operator float (Degree degree)
        {
            return degree.InnerValue;
        }
        #endregion
        
        #region Operator overloading
        public static bool operator ==(Degree a, Degree b)
        {
            return a.InnerValue == b.InnerValue;
        }
        public static bool operator !=(Degree a, Degree b)
        {
            return a.InnerValue != b.InnerValue;
        }
        public static Degree operator +(Degree a, Degree b)
        {
            return new Degree(a.InnerValue + b.InnerValue);
        }
        public static Degree operator -(Degree a, Degree b)
        {
            return new Degree(a.InnerValue - b.InnerValue);
        }
        public static Degree operator /(Degree a, float f)
        {
            return new Degree(a.InnerValue / f);
        }
        public static Degree operator *(Degree a, float f)
        {
            return new Degree(a.InnerValue * f);
        }
        public static Degree operator *(float f, Degree a)
        {
            return new Degree(a.InnerValue * f);
        }
        public static Degree operator -(Degree a)
        {
            return new Degree(-a.InnerValue);
        }
        #endregion
        #region override
        public override Int32 GetHashCode()
        {
            return InnerValue.GetHashCode();
        }
        public override Boolean Equals(Object obj)
        {
            return base.Equals(obj);
        }
        #endregion

        #region UnitTest
        public static void UnitTest()
        {
            for (int iter = 0; iter < 1000000; iter++)
            {

                Random rd = new Random();
                Degree lhs = rd.NextDegree(-(Degree)360, (Degree)360);
                Degree rhs = rd.NextDegree(-(Degree)360, (Degree)360);
                
                Debug.Assert((lhs + rhs).InnerValue == (float)(lhs.InnerValue + rhs.InnerValue));
            }
            /*
            Debug.Assert((lhs - rhs).InnerValue == lhs.InnerValue - rhs.InnerValue);
            Debug.Assert((f * lhs).InnerValue == f * lhs.InnerValue);
            Debug.Assert((lhs * f).InnerValue == f * lhs.InnerValue);
            Debug.Assert((lhs / f).InnerValue == f * lhs.InnerValue);
            Debug.Assert((-lhs).InnerValue == -(lhs.InnerValue));
            */
        }
        #endregion
    }
}
