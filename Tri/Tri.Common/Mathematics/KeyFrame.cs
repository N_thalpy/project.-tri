﻿namespace Tri.Mathematics
{
    public struct KeyFrame<T>
    {
        public Second Time;
        public T Value;

        public KeyFrame(Second time, T value)
        {
            Time = time;
            Value = value;
        }
    }
}
