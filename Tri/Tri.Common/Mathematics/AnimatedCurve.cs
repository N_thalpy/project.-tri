﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace Tri.Mathematics
{
    public enum InterpolationType
    {
        Linear,
        Quadratic,

        Custom,
    }

    public class AnimatedCurve<T> where T : struct
    {
        public delegate Tuple<float, float> WeightFunc(float weight);

        /// <summary>
        /// List of keyframe
        /// </summary>
        private List<KeyFrame<T>> keyFrame;

        private InterpolationType interType;
        private WeightFunc interFunc;

        private static Dictionary<InterpolationType, WeightFunc> interDic; 

        /// <summary>
        /// static .ctor
        /// </summary>
        static AnimatedCurve()
        {
            interDic = new Dictionary<InterpolationType, WeightFunc>();
            interDic.Add(InterpolationType.Linear, _ => new Tuple<float, float>(1 - _, _));
            interDic.Add(InterpolationType.Quadratic, _ => new Tuple<float, float>(1 - _ * _, _ * _));
        }

        /// <summary>
        /// .ctor
        /// </summary>
        public AnimatedCurve(InterpolationType interType)
        {
            keyFrame = new List<KeyFrame<T>>();
            this.interType = interType;

            Debug.Assert(interType != InterpolationType.Custom);
        }
        public AnimatedCurve(WeightFunc interFunc)
        {
            keyFrame = new List<KeyFrame<T>>();
            this.interType = InterpolationType.Custom;
            this.interFunc = interFunc;
        }

        /// <summary>
        /// Add keyframe
        /// </summary>
        /// <param name="time"></param>
        /// <param name="value"></param>
        public void AddKeyFrame(Second time, T value)
        {
            keyFrame.Add(new KeyFrame<T>(time, value));
            keyFrame.OrderBy(_ => _.Time);
        }

        /// <summary>
        /// Get next value of keyframe
        /// </summary>
        /// <returns></returns>
        public T GetValue(Second sec)
        {
            int index = 0;
            foreach (KeyFrame<T> frame in keyFrame)
            {
                if (frame.Time > sec)
                {
                    index--;
                    break;
                }

                index++;
            }

            if (index == -1)
                return keyFrame[0].Value;
            if (index == keyFrame.Count)
                return keyFrame[keyFrame.Count - 1].Value;

            var curr = keyFrame[index];
            var next = keyFrame[index + 1];
            return GetInterpolatedValue(curr.Value, next.Value, (sec - curr.Time) / (next.Time - curr.Time));
        }
        /// <summary>
        /// Get delta of keyframe
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public T GetDelta(Second from, Second to)
        {
            return (T)from.GetType()
                .GetMethod("op_Subtraction", new Type[]{ typeof(T), typeof(T) })
                .Invoke(null, new object[]{ from, to });
        }

        /// <summary>
        /// Get interpolated value between lhs and rhs
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        private T GetInterpolatedValue(T lhs, T rhs, float weight)
        {
            float lweight, rweight;
            Tuple<float, float> temp;

            if (interDic.ContainsKey(interType))
                temp = interDic[interType].Invoke(weight);
            else
                temp = interFunc.Invoke(weight);
            Debug.Assert(temp != null);
            
            lweight = temp.Item1;
            rweight = temp.Item2;

            try
            {
                Expression e = Expression.Add(
                                   Expression.Multiply(
                                       Expression.Constant(lhs), 
                                       Expression.Constant(lweight)),
                                   Expression.Multiply(
                                       Expression.Constant(rhs), 
                                       Expression.Constant(rweight)));
                
                return (T)Expression.Lambda(e).Compile().DynamicInvoke(null);
            }
            catch (Exception e)
            {
                throw new NotImplementedException(String.Format("Need to make hard-coded code for type {0}", typeof(T)), e); 
            }
        }
    }
}
