﻿using System;
using System.Diagnostics;

namespace Tri.SystemComponent
{
    /// <summary>
    /// Version of tri library.
    /// Major.Minor.Patch.Revision
    /// </summary>
    public struct TriVersion
    {
        public static TriVersion CurrentVersion = new TriVersion(1, 4, 1, "Azma");

        public readonly int MajorVersion;
        public readonly int MinorVersion;
        public readonly int PatchVersion;
        public readonly String Revision;

        public TriVersion(int major, int minor, int patch, String revision)
        {
            Debug.Assert(major > 0);
            Debug.Assert(minor >= 0);
            Debug.Assert(patch >= 0);
            Debug.Assert(String.IsNullOrWhiteSpace(revision) == false);

            MajorVersion = major;
            MinorVersion = minor;
            PatchVersion = patch;
            Revision = revision;
        }

        #region operator overloading
        public static bool operator == (TriVersion lhs, TriVersion rhs)
        {
            return lhs.MajorVersion == rhs.MajorVersion &&
                lhs.MinorVersion == rhs.MinorVersion &&
                lhs.PatchVersion == rhs.PatchVersion &&
                lhs.Revision == rhs.Revision;
        }
        public static bool operator != (TriVersion lhs, TriVersion rhs)
        {
            return (lhs == rhs) == false;
        }
        public static bool operator < (TriVersion lhs, TriVersion rhs)
        {
            if (lhs.MajorVersion != rhs.MajorVersion)
                return lhs.MajorVersion < rhs.MajorVersion;
            else if (lhs.MinorVersion != rhs.MinorVersion)
                return lhs.MinorVersion < rhs.MinorVersion;
            else
                return lhs.PatchVersion < rhs.PatchVersion;
        }
        public static bool operator > (TriVersion lhs, TriVersion rhs)
        {
            if (lhs.MajorVersion != rhs.MajorVersion)
                return lhs.MajorVersion > rhs.MajorVersion;
            else if (lhs.MinorVersion != rhs.MinorVersion)
                return lhs.MinorVersion > rhs.MinorVersion;
            else
                return lhs.PatchVersion > rhs.PatchVersion;
        }
        #endregion
        #region override
        public override Int32 GetHashCode()
        {
            return ToString().GetHashCode();
        }
        public override Boolean Equals(Object obj)
        {
            if (obj is TriVersion)
                return (TriVersion)obj == this;
            else
                return false;
        }
        public override String ToString()
        {
            return String.Format("V.{0}.{1}.{2} {3}", MajorVersion, MinorVersion, PatchVersion, Revision);
        }
        #endregion
    }
}
