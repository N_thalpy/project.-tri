﻿using System;

namespace Tri.Crypto.Codec
{
    public abstract class CodecBase
    {
        public abstract Byte[] Encode(Byte[] input);
        public abstract Byte[] Decode(Byte[] input);
    }
}
