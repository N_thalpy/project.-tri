﻿using System;

namespace Tri.Coroutine
{
    public class WaitForFrame : CoroutineElement
    {
        /// <summary>
        /// Is this element passable?
        /// </summary>
        public override Boolean Finished
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        /// <summary>
        /// Remained frame count
        /// </summary>
        private int frameRemain;

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="frameCount"></param>
        public WaitForFrame(int frameCount)
        {
            frameRemain = frameCount;
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void Update(Second deltaTime)
        {
            frameRemain--;
        }
    }
}
