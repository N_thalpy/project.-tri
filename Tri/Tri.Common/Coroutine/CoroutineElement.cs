﻿namespace Tri.Coroutine
{
    public abstract class CoroutineElement
    {
        /// <summary>
        /// Is this element finished?
        /// </summary>
        public abstract bool Finished
        {
            get;
        }

        /// <summary>
        /// Update
        /// </summary>
        public abstract void Update(Second deltaTime);
    }
}
