﻿using Tri.Mathematics;
using OpenTK;
using System.Collections.Generic;

namespace Tri.Physics
{
    public class Polygon
    {
        /// <summary>
        /// List of point in polygon
        /// </summary>
        public List<Vector2> PointList;

        /// <summary>
        /// Empty .ctor
        /// </summary>
        public Polygon()
        {
            PointList = new List<Vector2>();
        }
        /// <summary>
        /// .ctor w/ initial point list
        /// </summary>
        /// <param name="initPointList"></param>
        public Polygon(Vector2[] initPointList)
        {
            PointList = new List<Vector2>(initPointList);
        }
        /// <summary>
        /// .ctor w/ initial point list
        /// </summary>
        /// <param name="initPointList"></param>
        public Polygon(List<Vector2> initPointList)
        {
            PointList = initPointList;
        }

        /// <summary>
        /// Check point is inside polygon
        /// Code is from url
        /// http://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
        /// </summary>
        /// <param name="point">point to check</param>
        /// <returns></returns>
        public bool IsPointInside(Vector2 point)
        {
            float minX = PointList[0].X;
            float maxX = PointList[0].X;
            float minY = PointList[0].Y;
            float maxY = PointList[0].Y;
            for (int i = 1; i < PointList.Count; i++)
            {
                Vector2 q = PointList[i];
                minX = Mathf.Min(q.X, minX);
                maxX = Mathf.Max(q.X, maxX);
                minY = Mathf.Min(q.Y, minY);
                maxY = Mathf.Max(q.Y, maxY);
            }

            if (point.X < minX || point.X > maxX || point.Y < minY || point.Y > maxY)
            {
                return false;
            }

            // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
            bool inside = false;
            for (int i = 0, j = PointList.Count - 1; i < PointList.Count; j = i++)
            {
                if ((PointList[i].Y > point.Y) != (PointList[j].Y > point.Y) &&
                     point.X < (PointList[j].X - PointList[i].X) * (point.Y - PointList[i].Y) / (PointList[j].Y - PointList[i].Y) + PointList[i].X)
                {
                    inside = !inside;
                }
            }

            return inside;
        }
    }
}
