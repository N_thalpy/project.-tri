﻿using OpenTK;

namespace Tri.Physics
{
    public struct Transform
    {
        public Vector2 Position;
        public Degree Rotation;
    }
}
