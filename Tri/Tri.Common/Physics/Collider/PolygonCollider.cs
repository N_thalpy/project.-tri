﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Tri.Mathematics;

namespace Tri.Physics.Collider
{
    public class PolygonCollider : ColliderBase
    {
        /// <summary>
        /// Polygon of collider
        /// </summary>
        public Polygon polygon
        {
            get;
            protected set;
        }

        #region .ctor
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="polygon"></param>
        public PolygonCollider(Polygon polygon)
            : base()
        {
            this.polygon = polygon;
        }
        #endregion

        #region Polygon related Helper
        /// <summary>
        /// Helper function to process graham-scan method.
        /// https://en.wikipedia.org/wiki/Graham_scan
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns>positive if ccw, negative if cw, zero if linear</returns>
        protected float IsCounterClockwise(Vector2 p1, Vector2 p2, Vector2 p3)
        {
            return (p2.X - p1.X) * (p3.Y - p1.Y) - (p2.Y - p1.Y) * (p3.X - p1.X);
        }
        /// <summary>
        /// Generate convex polygon using pointList with graham-scan method.
        /// https://en.wikipedia.org/wiki/Graham_scan.
        /// warning: may cause inefficient memory performance
        /// </summary>
        /// <param name="pointList"></param>
        protected void GenerateConvexPolygon(List<Vector2> pointList)
        {
            polygon.PointList.Clear();

            pointList = pointList.OrderBy(_ => _.Y).ToList();
            Vector2 min = pointList[0];
            Debug.Assert(pointList.TrueForAll(_ => _.Y >= min.Y));

            pointList.RemoveAll(_ => _ == min);

            pointList = pointList.OrderBy(_ => Math.Atan2(_.Y - min.Y, _.X - min.X)).ToList();
            pointList.Insert(0, min);

            int pointCount = pointList.Count;
            Stack<Vector2> pointStack = new Stack<Vector2>();
            pointStack.Push(pointList[pointCount - 1]);
            pointStack.Push(pointList[0]);
            pointStack.Push(pointList[1]);

            int i = 2;
            while (i < pointCount)
            {
                Vector2 p2 = pointStack.Pop();
                Vector2 p1 = pointStack.Peek();
                pointStack.Push(p2);
                if (IsCounterClockwise(p1, p2, pointList[i]) >= 0)
                {
                    pointStack.Push(pointList[i]);
                    i++;
                }
                else
                    pointStack.Pop();
            }

            foreach (Vector2 p in pointStack)
                polygon.PointList.Add(p);
        }
        #endregion
        #region Collision Check
        /// <summary>
        /// Is this collider collided w/ other collider?
        /// </summary>
        /// <param name="op"></param>
        /// <returns></returns>
        public override Boolean IsCollided(ColliderBase op)
        {
            Debug.Assert(op != this);

            bool result = false;
#pragma warning disable CS0618
            if (op is CircleCollider)
            {
                CircleCollider cc = op as CircleCollider;
                foreach (Vector2 v2 in polygon.PointList)
                {
                    if ((v2 - cc.Position).Length < cc.Radius)
                    {
                        result = true;
                        break;
                    }
                }
            }
#pragma warning restore CS0618
            else if (op is PolygonCollider)
            {
                PolygonCollider pc = op as PolygonCollider;
                foreach (Vector2 v2 in this.polygon.PointList)
                {
                    if (pc.polygon.IsPointInside(v2))
                    {
                        result = true;
                        break;
                    }
                }
                if (result == false)
                    foreach (Vector2 v2 in pc.polygon.PointList)
                    {
                        if (this.polygon.IsPointInside(v2))
                        {
                            result = true;
                            break;
                        }
                    }
            }
            else
                throw new ArgumentException();
            
            return result;
        }
        #endregion
    }
}
