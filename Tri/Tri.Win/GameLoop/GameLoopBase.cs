﻿using System;
using System.Diagnostics;

namespace Tri.Win.GameLoop
{
    /// <summary>
    /// Status of GameLoop
    /// </summary>
    public enum GameLoopStatus
    {
        Created,
        Initialized,
        Exited,
        Disposed,
    }
    public abstract class GameLoopBase
    {
        /// <summary>
        /// Status of game loop
        /// </summary>
        public GameLoopStatus Status
        {
            get;
            private set;
        }
        /// <summary>
        /// Name of game loop
        /// </summary>
        public readonly String Name;
        
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="debugName"></param>
        public GameLoopBase(String debugName)
        {
            Status = GameLoopStatus.Created;
            Name = debugName;
        }
        /// <summary>
        /// .init
        /// </summary>
        public void Initialize()
        {
            OnInitialize();
            Debug.Assert(Status == GameLoopStatus.Created);
            Status = GameLoopStatus.Initialized;
        }
        protected virtual void OnInitialize()
        {
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="deltaTime"></param>
        public abstract void Update(Second deltaTime);
        /// <summary>
        /// Render
        /// </summary>
        /// <param name="deltaTime"></param>
        public abstract void Render(Second deltaTime);

        /// <summary>
        /// Finish the game loop
        /// </summary>
        public void Stop()
        {
            Debug.Assert(Status == GameLoopStatus.Initialized);
            OnStop();
            Status = GameLoopStatus.Exited;
        }
        protected virtual void OnStop()
        {
        }

        /// <summary>
        /// Dispose the game loop
        /// </summary>
        /// <param name="manual"></param>
        public virtual void Dispose()
        {
            Status = GameLoopStatus.Disposed;
        }
    }
}
