﻿using OpenTK;
using System;

namespace Tri.Win.Rendering.Camera
{
    /// <summary>
    /// Orthographic Camera.
    /// </summary>
    public class OrthographicCamera
    {
        public Matrix4 Matrix;
        
        /// <summary>
        /// Position of camera
        /// </summary>
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                if (value == position)
                    return;

                position = value;
                CalculateMatrix();
            }
        }
        private Vector2 position;
        /// <summary>
        /// Size of view
        /// </summary>
        public Vector2 Viewport
        {
            get
            {
                return viewport;
            }
            set
            {
                if (value == viewport)
                    return;

                viewport = value;
                CalculateMatrix();
            }
        }
        private Vector2 viewport;
        
        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="viewport"></param>
        /// <param name="position"></param>
        public OrthographicCamera(Vector2 viewport, Vector2 position)
        {
            this.position = position;
            this.viewport = viewport;
            CalculateMatrix();
        }

        /// <summary>
        /// Move camera position
        /// </summary>
        /// <param name="position"></param>
        private void CalculateMatrix()
        {
            Matrix4.CreateOrthographic(viewport.X, viewport.Y, 0, 10000f, out Matrix);
            Matrix4 view = Matrix4.CreateTranslation(new Vector3(-position.X, -position.Y, 0));
            Matrix4.Mult(ref view, ref Matrix, out Matrix);
        }

        public Vector3 ScreenToWorld(Vector2 screen, float z)
        {
            throw new NotImplementedException();
        }
        public Vector2 WorldToScreen(Vector3 world)
        {
            throw new NotImplementedException();
        }
    }
}
