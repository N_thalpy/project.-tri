﻿using Tri.Win.Rendering.Shader;

namespace Tri.Win.Rendering
{
    public static class ShaderProvider
    {
        static ScreenSpaceTextureShader screenSpaceTexture;
        public static ScreenSpaceTextureShader ScreenSpaceTexture
        {
            get
            {
                if (screenSpaceTexture == null)
                    screenSpaceTexture = new ScreenSpaceTextureShader();
                return screenSpaceTexture;
            }
        }

        static SimpleTextureShader simpleTexture;
        public static SimpleTextureShader SimpleTexture
        {
            get
            {
                if (simpleTexture == null)
                    simpleTexture = new SimpleTextureShader();
                return simpleTexture;
            }
        }
    }
}
