﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Tri.Win.Rendering
{
    public class Texture : IDisposable
    {
        /// <summary>
        /// Handle for OpenTK
        /// </summary>
        public int TextureHandle
        {
            get;
            private set;
        }
        /// <summary>
        /// Original size of texture
        /// </summary>
        public Vector2 Size
        {
            get;
            private set;
        }
        
        /// <summary>
        /// static .ctor
        /// </summary>
        static Texture()
        {
        }
        /// <summary>
        /// .ctor
        /// </summary>
        private Texture(int handle)
        {
            TextureHandle = handle;
        }

        /// <summary>
        /// Creates Texture from bitmap file
        /// </summary>
        /// <param name="fileName">Name of bitmap</param>
        /// <returns></returns>
        public static Texture CreateFromBitmap(Bitmap bitmap)
        {
            int id = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, id);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bitmapData.Width, bitmapData.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bitmapData.Scan0);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            bitmap.UnlockBits(bitmapData);

            GL.BindTexture(TextureTarget.Texture2D, 0);

            Texture tex = new Texture(id);
            tex.Size = new Vector2(bitmap.Width, bitmap.Height);
            return tex;
        }
        /// <summary>
        /// Creates Texture from bitmap file path
        /// </summary>
        /// <param name="fileName">Name of bitmap</param>
        /// <returns></returns>
        public static Texture CreateFromBitmapPath(String path)
        {
            if (File.Exists(path) == false)
                throw new FileNotFoundException(String.Format("File {0} has not found.", path));
            
            using (Bitmap bitmap = new Bitmap(path))
            {
                return CreateFromBitmap(new Bitmap(path));
            }
        }
        /// <summary>
        /// Creates Texture from bitmap file path asyncronously.
        /// </summary>
        /// <returns>Texture</returns>
        /// <param name="path">Name of bitmap</param>
        public static Task<Texture> CreateFromBitmapPathAsync(String path)
        {
            return Task.FromResult(Texture.CreateFromBitmapPath(path));
        }
        /// <summary>
        /// Disposes texture
        /// </summary>
        public void Dispose()
        {
            if (TextureHandle != 0)
                GL.DeleteTexture(TextureHandle);
            TextureHandle = 0;
        }
    }
}
