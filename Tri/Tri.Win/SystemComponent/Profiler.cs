﻿using Tri.Win.UI;
using OpenTK;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Linq;
using Tri.Win.Rendering;
using Tri.Win.UI.BitmapFont;
using System.Collections.Generic;

namespace Tri.Win.SystemComponent
{
    public class Profiler : IDisposable
    {
        /// <summary>
        /// TextView to display profiled data
        /// </summary>
        private BitmapLabel label;

        /// <summary>
        /// Last deltaTime of update tick
        /// </summary>
        private Second lastDeltaTime;
        private List<Second> deltaTimeList;

        /// <summary>
        /// Max recommended memory usage.
        /// SHOULD check memory leakage or wrong optimization when memory usage is over this value.
        /// </summary>
        private const long WarningMemoryUsage = 1000 * 1000 * 1000;

        /// <summary>
        /// StringBuilder (for caching)
        /// </summary>
        private StringBuilder sb;

        /// <summary>
        /// .ctor
        /// </summary>
        public Profiler()
        {
            label = new BitmapLabel()
            {
                Position = new Vector2(0, 0),
                Anchor = new Vector2(0, 0),
                Depth = 0,
                BitmapFont = BitmapFont.Create("SystemResources/arial.fnt"),
                Color = Color.White
            };
            lastDeltaTime = (Second)0f;
            deltaTimeList = new List<Second>();
            sb = new StringBuilder();
        }
        
        /// <summary>
        /// Get frame per second
        /// </summary>
        /// <returns>fps</returns>
        public float GetFPS()
        {
            if (lastDeltaTime <= 0f)
                return float.NaN;
            else
                return 1 / lastDeltaTime;
        }
        /// <summary>
        /// Get current memory usage
        /// </summary>
        /// <returns>current memory usage</returns>
        public long GetMemoryUsage()
        {
            long memuse = Process.GetCurrentProcess().PrivateMemorySize64;
            if (memuse > WarningMemoryUsage)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Memory Usage is {0} now. Should check memory optimization", memuse);

            return memuse;
        }

        /// <summary>
        /// Returns profiling message
        /// </summary>
        /// <returns></returns>
        public String GetProfileMessage()
        {
            sb.Clear();
            sb.AppendLine(String.Format("Tri.Win Library {0}", GameSystem.GameVersion));
            sb.AppendLine(String.Format("FPS: {0:F1}", GetFPS()));
            sb.AppendLine(String.Format("Frame Interval: {0:F2}ms (Max {1:F2}ms, Min {2:F2}ms)", 
                1000 * (float)lastDeltaTime, 
                1000 * (float)deltaTimeList.Max(_ => (float)_),
                1000 * (float)deltaTimeList.Min(_ => (float)_)));
            sb.AppendLine(String.Format("Memory: {0}MB", GetMemoryUsage() / (1000 * 1000)));

            return sb.ToString().Trim();
        }

        /// <summary>
        /// Update profiler
        /// </summary>
        /// <param name="deltaTime"></param>
        public void Update(Second deltaTime)
        {
            lastDeltaTime = deltaTime;
            deltaTimeList.Add(deltaTime);
            if (deltaTimeList.Count > 60)
                deltaTimeList.RemoveAt(0);
        }
        /// <summary>
        /// Render profiler
        /// </summary>
        public void Render()
        {
            if (GameSystem.IsDebug)
            {
                label.Text = GetProfileMessage();
                label.Render(ref GameSystem.DebugCamera.Matrix);
            }
        }

        /// <summary>
        /// Dispose Profiler
        /// </summary>
        public void Dispose()
        {
            label.Dispose();
        }
    }
}
