﻿using OpenTK;
using OpenTK.Input;

namespace Tri.Win.SystemComponent
{
    /// <summary>
    /// OpenTK input helper/wrapper
    /// </summary>
    public static class InputHelper
    {
        /// <summary>
        /// previous keyboard state
        /// </summary>
        private static KeyboardState prevKeyboardState;
        /// <summary>
        /// current keyboard state
        /// </summary>
        private static KeyboardState currentKeyboardState;

        /// <summary>
        /// previous mouse state
        /// </summary>
        private static MouseState prevMouseState;
        /// <summary>
        /// current mouse state
        /// </summary>
        private static MouseState currentMouseState;

        /// <summary>
        /// interface for get current keyboard state
        /// </summary>
        public static KeyboardState KeyboardState
        {
            get
            {
                return currentKeyboardState;
            }
        }
        /// <summary>
        /// interface for get current mouse state
        /// </summary>
        public static MouseState MouseState
        {
            get
            {
                return currentMouseState;
            }
        }

        /// <summary>
        /// Returns delta position of mouse
        /// </summary>
        public static Vector2 MouseDeltaPosition
        {
            get
            {
                float dx = currentMouseState.X - prevMouseState.X;
                float dy = currentMouseState.Y - prevMouseState.Y;

                return new Vector2(dx, dy);
            }
        }

        /// <summary>
        /// static .ctor
        /// </summary>
        static InputHelper()
        {
            prevKeyboardState = new KeyboardState();
            currentKeyboardState = new KeyboardState();
            prevMouseState = new MouseState();
            currentMouseState = new MouseState();
        }

        /// <summary>
        /// Update state
        /// </summary>
        public static void Update()
        {
            prevKeyboardState = currentKeyboardState;
            prevMouseState = currentMouseState;

            currentKeyboardState = Keyboard.GetState();
            currentMouseState = Mouse.GetCursorState();
        }

        #region Helper
        public static bool IsPressed(Key k)
        {
            return prevKeyboardState.IsKeyUp(k) && currentKeyboardState.IsKeyDown(k);
        }
        public static bool IsDown(Key k)
        {
            return currentKeyboardState.IsKeyDown(k);
        }
        public static bool IsReleased(Key k)
        {
            return prevKeyboardState.IsKeyDown(k) && currentKeyboardState.IsKeyUp(k);
        }

        public static bool IsPressed(MouseButton k)
        {
            return prevMouseState.IsButtonUp(k) && currentMouseState.IsButtonDown(k);
        }
        public static bool IsDown(MouseButton k)
        {
            return currentMouseState.IsButtonDown(k);
        }
        public static bool IsReleased(MouseButton k)
        {
            return prevMouseState.IsButtonDown(k) && currentMouseState.IsButtonUp(k);
        }
        #endregion
    }
}
