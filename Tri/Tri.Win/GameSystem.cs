﻿using System;
using System.Reflection;
using OpenTK;
using OpenTK.Audio;
using Tri.SystemComponent;
using Tri.Coroutine;
using Tri.Win.GameLoop;
using Tri.Win.Rendering.Camera;
using Tri.Win.SystemComponent;

namespace Tri.Win
{
    /// Program.Main    -- calls --> GameSystem.Run
    /// GameSystem.Run  -- calls --> MainForm.Run (which overrides GameWindow.Run)
    /// MainForm.Run    -- calls --> GameSystem.UpdateTick & GameSystem.RenderTick
    public static class GameSystem
    {
        #region public get/private set variables
        public static TriVersion GameVersion
        {
            get;
            private set;
        }
        public static bool IsDebug;
        public static bool DoUnitTest;

        public static MainForm MainForm
        {
            get;
            private set;
        }
        public static Profiler Profiler
        {
            get;
            private set;
        }
        public static LogSystem LogSystem
        {
            get;
            private set;
        }
        public static AudioContext AudioContext
        {
            get;
            private set;
        }
        public static UITracer UITracer
        {
            get;
            private set;
        }
        public static PolygonTracer PolygonTracer
        {
            get;
            private set;
        }
        public static CoroutineManager CoroutineManager
        {
            get;
            private set;
        }

        public static GameTime UpdateTime
        {
            get;
            private set;
        }
        public static GameTime RenderTime
        {
            get;
            private set;
        }

        public static OrthographicCamera DebugCamera
        {
            get;
            private set;
        }

        public static GameLoopBase CurrentGameLoop
        {
            get;
            private set;
        }
        #endregion

        #region .ctor/init
        /// <summary>
        /// static .ctor
        /// </summary>
        static GameSystem()
        {
            GameVersion = TriVersion.CurrentVersion;
            DoUnitTest = true;
        }
        /// <summary>
        /// Initializer
        /// </summary>
        public static void Initialize(int width, int height, String name)
        {
            UpdateTime = new GameTime();
            RenderTime = new GameTime();

            MainForm = new MainForm(width, height, name);
            Profiler = new Profiler();
            LogSystem = new LogSystem();
            AudioContext = new AudioContext();
            UITracer = new UITracer();
            PolygonTracer = new PolygonTracer();
            CoroutineManager = new CoroutineManager();

            DebugCamera = new OrthographicCamera(MainForm.Viewport, 0.5f * MainForm.Viewport);

            // Run Unit Test
            if (DoUnitTest == true)
            {
                Assembly asm = Assembly.GetAssembly(typeof(GameSystem));
                foreach (Type t in asm.GetTypes())
                    if (t.GetCustomAttribute(typeof(UnitTestAttribute)) != null)
                    {
                        GameSystem.LogSystem.WriteLine(LogType.Info, "Starting Unit Test: {0}... ", t.FullName);
                        t.GetMethod("UnitTest").Invoke(null, null);
                    }
            }
        }
        #endregion

        #region Game related
        public static void LoadResources(Assembly asm)
        {
            foreach (Type t in asm.GetTypes())
            {
                var lr = t.GetMethod("LoadResource", new Type[]{ });
                if (lr != null)
                    lr.Invoke(null, null);
            }
        }
        /// <summary>
        /// Assign Game Loop to loop
        /// </summary>
        /// <param name="gameLoopBase"></param>
        public static void RunGameLoop(GameLoopBase gameLoopBase)
        {
            CurrentGameLoop = gameLoopBase;
            CurrentGameLoop.Initialize();
        }
        /// <summary>
        /// Run game.
        /// </summary>
        public static void Run()
        {
            // Initialize UpdateTime and RenderTime before enter the game
            UpdateTime.Refresh();
            RenderTime.Refresh();

            MainForm.Run();
        }
        /// <summary>
        /// Exit Game
        /// </summary>
        public static void Exit()
        {
            Environment.Exit(0);
        }
        #endregion
        #region Tick related
        /// <summary>
        /// Update game. Called for each update tick
        /// </summary>
        public static void UpdateTick()
        {
            UpdateTime.Refresh();
            Second deltaTime = UpdateTime.DeltaTime;

            Profiler.Update(deltaTime);
            CoroutineManager.Update(deltaTime);
            UITracer.Update();
            PolygonTracer.Update();
            InputHelper.Update();

            CurrentGameLoop.Update(deltaTime);
        }
        /// <summary>
        /// Render game. Called for each rendering tick
        /// </summary>
        public static void RenderTick()
        {
            RenderTime.Refresh();
            Second deltaTime = RenderTime.DeltaTime;

            CurrentGameLoop.Render(deltaTime);
            
            Profiler.Render();
            LogSystem.Render();
            UITracer.Render();
            PolygonTracer.Render();
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Dispose Game System
        /// </summary>
        public static void Dispose()
        {
            if (Profiler != null)
                Profiler.Dispose();
            if (LogSystem != null)
                LogSystem.Dispose();
            if (AudioContext != null)
                AudioContext.Dispose();
        }
        #endregion
    }
}
