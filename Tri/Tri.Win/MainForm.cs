﻿using Tri.Win.Rendering;
using Tri.Win.SystemComponent;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Drawing;
using Tri.Win.Rendering.Renderer;

namespace Tri.Win
{
    public class MainForm : GameWindow
    {
        public Vector2 Viewport
        {
            get;
            private set;
        }

        /// <summary>
        /// .ctor
        /// </summary>
        public MainForm(int width, int height, String name)
            : base(width, height, new GraphicsMode(32, 24, 0, 4), name, 
                  GameWindowFlags.FixedWindow, DisplayDevice.Default,
                  3, 2, GraphicsContextFlags.ForwardCompatible)
        {
            Viewport = new Vector2(width, height);
        }

        /// <summary>
        /// Update Objects.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            GameSystem.UpdateTick();
        }
        /// <summary>
        /// Renders Objects.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            
            GL.ClearColor(Color4.CornflowerBlue);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.Blend);
            
            GameSystem.RenderTick();

            SwapBuffers();
            GLHelper.CheckGLError();
        }

        /// <summary>
        /// Calculate current mouse position based on MainForm window
        /// </summary>
        /// <returns></returns>
        public Vector2 GetMousePosition()
        {
            MouseState cursor = InputHelper.MouseState;
            Point scrPnt = new Point(cursor.X, cursor.Y);
            Point cliPnt = PointToClient(scrPnt);
            return new Vector2(cliPnt.X, cliPnt.Y);
        }
    }
}
