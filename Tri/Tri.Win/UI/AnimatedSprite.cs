﻿using OpenTK;
using OpenTK.Graphics;
using System.Diagnostics;
using Tri.Win.Rendering;
using Tri.Win.Rendering.Renderer;
using Tri.Win.SystemComponent;
using System.Drawing;

namespace Tri.Win.UI
{
    public class AnimatedSprite : View
    {
        public Texture[] TextureList;
        public Second FrameInterval;
        private int textureIndex;

        private Second currentTime;

        public AnimatedSprite()
        {
            TextureList = null;
            FrameInterval = Second.Zero;
            textureIndex = 0;
        }

        public void Reset()
        {
            textureIndex = 0;
        }

        protected override void OnRender(ref Matrix4 mat)
        {
            Debug.Assert(FrameInterval != Second.Zero);
            base.OnRender(ref mat);

            Texture tex = TextureList[textureIndex];
            if (Size == Vector2.Zero)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Size of sprite is (0, 0)");
            if (tex == null)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Texture of sprite is null");
            else if (tex.TextureHandle == 0)
                GameSystem.LogSystem.WriteLine(LogType.Error, "Texture is disposed");

            using (BillboardRenderer br = new BillboardRenderer(1))
            {
                br.Begin();
                br.Render(new RenderParameter()
                {
                    Position = new Vector3(Position.X, Position.Y, -Depth),
                    Anchor = Anchor,
                    Size = Size,
                    Angle = Angle,
                    Color = Color4.White,
                    TextureUV = new RectangleF(0, 0, 1, 1),
                });
                br.End(ref mat, tex);
            }
        }
        protected override void OnUpdate(Second deltaTime)
        {
            Debug.Assert(FrameInterval != Second.Zero);
            base.OnUpdate(deltaTime);

            currentTime += deltaTime;
            while (currentTime > FrameInterval)
            {
                currentTime -= FrameInterval;
                textureIndex = (textureIndex + 1) % TextureList.Length;
            }
        }
    }
}
