﻿using System;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics;
using Tri.Win.Rendering;
using Tri.Win.Rendering.Renderer;
using Tri.Win.UI.BitmapFont;
using System.Drawing;
using System.IO;

namespace Tri.Win.UI
{
    public class BitmapLabel : View
    {
        private bool isTextureDirty;

        private Texture[] tex;

        private BitmapFont.BitmapFont bitmapFont;
        public BitmapFont.BitmapFont BitmapFont
        {
            get
            {
                return bitmapFont;
            }
            set
            {
                if (bitmapFont == value)
                    return;

                bitmapFont = value;
                isTextureDirty = true;
            }
        }

        public float FontSize;
        public String Text;
        public Color4 Color;

        public BitmapLabel()
            : base()
        {
            isTextureDirty = false;
            FontSize = 12;
        }

        protected override void OnUpdate(Second deltaTime)
        {
            base.OnUpdate(deltaTime);
        }
        protected override void OnRender(ref Matrix4 mat)
        {
            if (isTextureDirty == true)
            {
                if (tex != null)
                    foreach (Texture t in tex)
                        t.Dispose();

                int count = 0;
                foreach (String s in Directory.EnumerateFiles(Path.GetDirectoryName(bitmapFont.LoadedFontName)))
                {
                    String s2 = s.Replace("\\", "/");
                    if (s2.StartsWith(bitmapFont.LoadedFontName))
                    if (s2.EndsWith(".png"))
                        count++;
                }

                Debug.Assert(count > 0);
                tex = new Texture[count];

                if (count == 1)
                    tex[0] = Texture.CreateFromBitmapPath(String.Format("{0}.png", bitmapFont.LoadedFontName));
                else
                    for (int i = 0; i < count; i++)
                        tex[i] = Texture.CreateFromBitmapPath(String.Format("{0}_{1}.png", bitmapFont.LoadedFontName, i));
                    
                isTextureDirty = false;
            }
            using (BillboardRenderer br = new BillboardRenderer(Text.Length))
            {
                float fontScale = (float)FontSize / bitmapFont.FontSize;
                Size measure = bitmapFont.MeasureFont(Text);
                Vector2 initpos = new Vector2(Position.X - Anchor.X * measure.Width * fontScale, Position.Y + (1 - Anchor.Y) * measure.Height * fontScale);
                Vector2 pos = initpos;

                for (int pageIndex = 0; pageIndex < tex.Length; pageIndex++)
                {
                    pos = initpos;
                    br.Begin();
                    for (int idx = 0; idx < Text.Length; idx++)
                    {
                        int kern = 0;
                        if (idx != Text.Length - 1)
                            bitmapFont.GetKerning(Text[idx], Text[idx + 1]);

                        if (Text[idx] == '\n')
                        {
                            pos.X = initpos.X;
                            pos.Y -= bitmapFont.LineHeight * fontScale;
                        }
                        else if (Text[idx] == '\r')
                        {
                            // Do nothing
                        }
                        else
                        {
                            Character c = bitmapFont.Characters[Text[idx]];

                            if (c.TexturePage == pageIndex)
                                br.Render(new RenderParameter()
                                {
                                    Position = new Vector3(pos.X + (c.Offset.X + kern) * fontScale, pos.Y - c.Offset.Y * fontScale, -Depth),
                                    Anchor = new Vector2(0f, 1f),
                                    Size = new Vector2(c.Bounds.Width * fontScale, c.Bounds.Height * fontScale),
                                    Angle = Angle,
                                    Color = Color,
                                    TextureUV = new RectangleF(c.Bounds.X / tex[c.TexturePage].Size.X, c.Bounds.Y / tex[c.TexturePage].Size.Y, 
                                        c.Bounds.Width / tex[c.TexturePage].Size.X, c.Bounds.Height / tex[c.TexturePage].Size.Y),
                                });

                            pos.X += (c.XAdvance + kern) * fontScale;
                        }
                    }
                    br.End(ref mat, tex[pageIndex]);
                }
            }
        }
        public override void Dispose()
        {
            base.Dispose();
            if (tex != null)
                foreach (Texture t in tex)
                    t.Dispose();
        }
    }
}

