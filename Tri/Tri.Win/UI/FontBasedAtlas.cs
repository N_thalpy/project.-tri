﻿using HAJE.VE.Rendering;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace HAJE.VE.UI
{
    public class FontBasedAtlas
    {
        private Dictionary<Char, Texture> charToTexture;
        public Font Font;

        public Texture GetTexture(Char ch)
        {
            if (charToTexture.ContainsKey(ch) == false)
            {
                Graphics g = Graphics.FromHwnd(GameSystem.MainForm.WindowInfo.Handle);
                RectangleF stringSize;

                if (String.IsNullOrEmpty(ch.ToString()) == true)
                    stringSize = new RectangleF(1, 1);
                else
                    stringSize = g.MeasureCharacterRanges(ch.ToString(), Font,
                        new RectangleF(0, 0, 1000, 1000), StringFormat.GenericDefault)[0].GetBounds();

                using (Bitmap bitmap = new Bitmap((int)stringSize.Width, (int)stringSize.Height))
                {
                    using (Graphics gfx = Graphics.FromImage(bitmap))
                    {
                        PointF pt = new PointF(0.0f, 0.0f);

                        gfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        gfx.Clear(Color.Transparent);
                        gfx.DrawString(ch.ToString(), Font, new SolidBrush(Color.White), pt);
                    }
                    
                    charToTexture.Add(ch, Texture.CreateFromBitmap(bitmap));
                }
            }
            
            return charToTexture[ch];
        }
        public FontBasedAtlas(Font font)
        {
            charToTexture = new Dictionary<char, Texture>();
            this.Font = font;
        }
    }
}
