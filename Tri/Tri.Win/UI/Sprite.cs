﻿using OpenTK;
using OpenTK.Graphics;
using Tri.Win.Rendering;
using Tri.Win.Rendering.Renderer;
using Tri.Win.SystemComponent;
using System.Drawing;

namespace Tri.Win.UI
{
    public class Sprite : View
    {
        public Color Color;
        public Texture Texture;

        /// <summary>
        /// .ctor
        /// </summary>
        public Sprite()
            : base()
        {
            Color = Color.White;
        }

        /// <summary>
        /// Draws sprite
        /// </summary>
        protected override void OnRender(ref Matrix4 mat)
        {
            if (Size == Vector2.Zero)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Size of sprite is (0, 0)");
            if (Texture == null)
                GameSystem.LogSystem.WriteLine(LogType.Warning, "Texture of sprite is null");
            else if (Texture.TextureHandle == 0)
                GameSystem.LogSystem.WriteLine(LogType.Error, "Texture is disposed");

            using (BillboardRenderer br = new BillboardRenderer(1))
            {
                br.Begin();
                br.Render(new RenderParameter()
                {
                    Position = new Vector3(Position.X, Position.Y, -Depth),
                    Anchor = Anchor,
                    Size = Size,
                    Angle = Angle,
                    Color = Color,
                    TextureUV = new RectangleF(0, 0, 1, 1),
                });
                br.End(ref mat, Texture);
            }
        }
    }
}
