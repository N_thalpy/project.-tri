﻿using Tri.Physics;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Tri.Win.UI
{
    /// <summary>
    /// delegate for transform change event
    /// </summary>
    /// <param name="sender">button which calls delegate</param>
    /// <param name="eventArgs">null</param>
    public delegate void OnTransformChange(View sender, object eventArgs);
    public abstract class View : IDisposable
    {
        #region public Variables
        /// <summary>
        /// Parent of view
        /// </summary>
        public View Parent
        {
            get;
            private set;
        }
        /// <summary>
        /// Is this view visible?
        /// </summary>
        public bool IsVisible;
        /// <summary>
        /// Is this view enable?
        /// </summary>
        public bool IsEnabled;

        /// <summary>
        /// Is this view disposed?
        /// </summary>
        public bool IsDisposed
        {
            get;
            private set;
        }

        /// <summary>
        /// Name for debugging
        /// </summary>
        public String DebugName;

        /// <summary>
        /// List of children
        /// </summary>
        public List<View> Children;

        /// <summary>
        /// Hitbox of view
        /// </summary>
        public Polygon Hitbox
        {
            get;
            private set;
        }

        /// <summary>
        /// Depth of view. May cause data racing when depth is same and texture is overlapped
        /// </summary>
        public float Depth
        {
            get
            {
                return depth;
            }
            set
            {
                if (depth == value)
                    return;

                depth = value;
                if (OnTransformChanged != null)
                    OnTransformChanged(this, null);
            }
        }
        private float depth;

        /// <summary>
        /// Position
        /// </summary>
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                if (position == value)
                    return;

                position = value;
                if (OnTransformChanged != null)
                    OnTransformChanged(this, null);
            }
        }
        private Vector2 position;

        /// <summary>
        /// Local position. Same with Position when parent is null.
        /// </summary>
        public Vector2 LocalPosition
        {
            get
            {
                if (Parent == null)
                    return Position;
                else
                    return Position - Parent.position;
            }
            set
            {
                if (Parent == null)
                    Position = value;
                else
                    Position = value + Parent.position;
            }
        }

        /// <summary>
        /// Position of Anchor
        /// </summary>
        public Vector2 Anchor
        {
            get
            {
                return anchor;
            }
            set
            {
                if (anchor == value)
                    return;

                anchor = value;
                if (OnTransformChanged != null)
                    OnTransformChanged(this, null);
            }
        }
        private Vector2 anchor;

        /// <summary>
        /// Size of View
        /// Size = new Vector2(Width, Height)
        /// </summary>
        public Vector2 Size
        {
            get
            {
                return size;
            }
            set
            {
                if (size == value)
                    return;

                size = value;
                if (OnTransformChanged != null)
                    OnTransformChanged(this, null);
            }
        }
        private Vector2 size;

        /// <summary>
        /// Angle of View. ccw
        /// </summary>
        public Degree Angle
        {
            get
            {
                return angle;
            }
            set
            {
                if (angle == value)
                    return;

                angle = value;
                if (OnTransformChanged != null)
                    OnTransformChanged(this, null);
            }
        }
        private Degree angle;
        #endregion
        #region event
        /// <summary>
        /// Called when transform (position/anchor/size/angle) has changed
        /// </summary>
        public event OnTransformChange OnTransformChanged;
        #endregion

        #region .ctor
        /// <summary>
        /// .ctor
        /// </summary>
        public View()
        {
            Position = Vector2.Zero;
            Anchor = new Vector2(0.5f, 0.5f);
            Size = Vector2.Zero;
            Angle = Degree.Zero;
            Depth = 1;

            DebugName = String.Empty;

            IsEnabled = true;
            IsVisible = true;

            Parent = null;
            Children = new List<View>();
        }
        #endregion
        #region Child related functions
        /// <summary>
        /// Add view as child
        /// </summary>
        /// <param name="child"></param>
        public void AddChild(View child)
        {
            Debug.Assert(child.Parent == null);
            Children.Add(child);
            child.Parent = this;
        }
        /// <summary>
        /// Remove child
        /// </summary>
        /// <param name="child"></param>
        public void RemoveChild(View child)
        {
            Debug.Assert(Children.Contains(child));
            Children.Remove(child);
            child.Parent = null;
        }
        #endregion
        #region Transform related functions
        /// <summary>
        /// Move View with Children
        /// </summary>
        /// <param name="vec"></param>
        public void Translate(Vector2 vec)
        {
            Position += vec;
            foreach (View child in Children)
                child.Translate(vec);
        }
        /// <summary>
        /// Rotate view with Children
        /// </summary>
        /// <param name="deg"></param>
        public void Rotate(Degree deg)
        {
            Angle += deg;

            foreach (View child in Children)
                child.InnerRotate(deg, this);
        }
        /// <summary>
        /// Resize view
        /// </summary>
        /// <param name="coeff"></param>
        public void Resize(float coeff)
        {
            this.size *= coeff;
            foreach (View child in Children)
                child.Resize(coeff);
        }
        /// <summary>
        /// Inner rotate function
        /// </summary>
        /// <param name="deg"></param>
        /// <param name="parent"></param>
        private void InnerRotate(Degree deg, View parent)
        {
            Angle += deg;

            Position = (Position - parent.Position).Rotate(deg) + parent.Position;
            foreach (View child in Children)
                child.InnerRotate(deg, parent);
        }
        
        /// <summary>
        /// re-calculate hitbox polygon
        /// </summary>
        public void CalculateHitbox()
        {
            float leftwidth = -Anchor.X * Size.X;
            float rightwidth = (1 - Anchor.X) * Size.X;
            float downheight = Anchor.Y * Size.Y;
            float upheight = -(1 - Anchor.Y) * Size.Y;

            Vector2 A = new Vector2(leftwidth, upheight).Rotate(Angle) + Position;
            Vector2 B = new Vector2(rightwidth, upheight).Rotate(Angle) + Position;
            Vector2 C = new Vector2(rightwidth, downheight).Rotate(Angle) + Position;
            Vector2 D = new Vector2(leftwidth, downheight).Rotate(Angle) + Position;

            Hitbox = new Polygon(new Vector2[4] { A, B, C, D });
        }
        #endregion

        #region Update/Render
        /// <summary>
        /// Update view
        /// </summary>
        /// <param name="deltaTime"></param>
        public void Update(Second deltaTime)
        {
            Debug.Assert(IsDisposed == false);
            if (IsEnabled == true)
                OnUpdate(deltaTime);

            foreach (View v in Children)
                v.Update(deltaTime);
        }
        /// <summary>
        /// Inner update code. will be overrided
        /// </summary>
        /// <param name="deltaTime"></param>
        protected virtual void OnUpdate(Second deltaTime)
        {
        }

        /// <summary>
        /// Renders view
        /// </summary>
        public void Render(ref Matrix4 mat)
        {
            Debug.Assert(IsDisposed == false);
            if (IsVisible == true && IsEnabled == true)
                OnRender(ref mat);

            foreach (View v in Children)
                v.Render(ref mat);
        }
        /// <summary>
        /// Inner rendering code. will be overrided
        /// </summary>
        protected virtual void OnRender(ref Matrix4 mat)
        {
        }
        #endregion
        #region Dispose
        /// <summary>
        /// Default dispose
        /// </summary>
        public virtual void Dispose()
        {
            Debug.Assert(IsDisposed == false);
            IsDisposed = true;
            foreach (View v in Children)
                v.Dispose();
        }
        #endregion
    }
}
