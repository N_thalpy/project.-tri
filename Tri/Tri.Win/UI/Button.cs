﻿using Tri.Win.Rendering;
using Tri.Win.SystemComponent;
using OpenTK;
using OpenTK.Input;
using Tri.Win.Rendering.Renderer;
using System;
using OpenTK.Graphics;
using System.Drawing;

namespace Tri.Win.UI
{
    /// <summary>
    /// delegate for button event
    /// </summary>
    /// <param name="sender">button which calls delegate</param>
    /// <param name="eventArgs">mouse state when delegate is called</param>
    public delegate void MouseAction(Button sender, MouseState eventArgs);

    public enum ButtonState
    {
        Idle,
        Pressed,
    }
    public class Button : View
    {
        /// Flow of Action call:
        ///
        /// OnClick() -> OnPress() -> OnRelease()
        /// 
        /// <summary>
        /// Will be invoked when button is clicked
        /// </summary>
        public event MouseAction OnClick;
        /// <summary>
        /// Will be invoked when button is pressing
        /// </summary>
        public event MouseAction OnPress;
        /// <summary>
        /// Will be invoked when button is released
        /// </summary>
        public event MouseAction OnRelease;

        /// <summary>
        /// State of button
        /// </summary>
        public ButtonState ButtonState
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Texture when button is on idle status
        /// </summary>
        public Texture IdleTex;
        /// <summary>
        /// Texture when button is on pressed status
        /// </summary>
        public Texture PressedTex;

        /// <summary>
        /// .ctor
        /// </summary>
        public Button()
            : base()
        {
            this.OnTransformChanged += (s, e) =>
            {
                s.CalculateHitbox();
            };
        }

        /// <summary>
        /// Check mouse is pressing button
        /// </summary>
        /// <param name="deltaTime"></param>
        protected override void OnUpdate(Second deltaTime)
        {
            if (InputHelper.IsReleased(MouseButton.Left))
            {
                if (ButtonState == ButtonState.Pressed)
                    if (OnRelease != null)
                        OnRelease(this, InputHelper.MouseState);
                ButtonState = ButtonState.Idle;
            }

            if (ButtonState == ButtonState.Pressed)
                if (OnPress != null)
                    OnPress(this, InputHelper.MouseState);

            if (InputHelper.IsPressed(MouseButton.Left))
            {
                if (Hitbox.IsPointInside(GameSystem.MainForm.GetMousePosition()))
                {
                    ButtonState = ButtonState.Pressed;
                    if (OnClick != null)
                        OnClick(this, InputHelper.MouseState);
                }
            }
        }

        /// <summary>
        /// Renders button
        /// </summary>
        protected override void OnRender(ref Matrix4 mat)
        {
            using (BillboardRenderer br = new BillboardRenderer(1))
            {
                br.Begin();
                br.Render(new RenderParameter()
                {
                    Position = new Vector3(Position.X, Position.Y, -Depth),
                    Anchor = Anchor,
                    Size = Size,
                    Angle = Angle,
                    Color = Color4.White,
                    TextureUV = new RectangleF(0, 0, 1, 1),
                });
                switch (ButtonState)
                {
                    case ButtonState.Idle:
                        br.End(ref mat, IdleTex);
                        break;

                    case ButtonState.Pressed:
                        br.End(ref mat, PressedTex);
                        break;

                    default:
                        throw new ArgumentException();
                }
            }
        }
    }
}
