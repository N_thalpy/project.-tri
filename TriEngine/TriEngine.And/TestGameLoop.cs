﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using Tri;
using Tri.And;
using Tri.And.GameLoop;
using Tri.And.Rendering;
using Tri.And.Rendering.Camera;
using Tri.And.Rendering.Renderer;
using Tri.And.UI;

namespace TriEngine.And
{
    public class TestGameLoop : GameLoopBase
    {
        Texture tex;
        List<RenderParameter> rpList;

        OrthographicCamera ocam;

        BillboardRenderer br;


        Random rd;

        public TestGameLoop()
            : base("_TestGameLoop")
        {
            rpList = new List<RenderParameter>();
            rd = new Random();
        }

        protected override void OnInitialize()
        {
            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport / 2);
            tex = Texture.CreateFromBitmapPath("brushed_steel.png");

            br = new BillboardRenderer(1000);
            for (int i = 0; i < 1000; i++)
                rpList.Add(new RenderParameter()
                {
                    Position = new Vector3(GameSystem.MainForm.Viewport.X * rd.NextFloat(), GameSystem.MainForm.Viewport.Y * rd.NextFloat(), 0),
                    Size = new Vector2(40, 40),
                    Angle = rd.NextDegree(),
                    TextureUV = new RectangleF(0, 0, 1, 1),
                    Color = new Color4(rd.NextFloat(), rd.NextFloat(), rd.NextFloat(), rd.NextFloat()),
                });
        }

        public override void Update(Second deltaTime)
        {
        }

        public override void Render(Second deltaTime)
        {
            br.Begin();
            foreach (RenderParameter rp in rpList)
                br.Render(rp);
            br.End(ref ocam.Matrix, tex);
        }
    }
}

