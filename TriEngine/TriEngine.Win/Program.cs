﻿using System;
using Tri.Win;
using TriEngine.Win.GameLoop;

namespace Tri.WinTest
{
    static class Program
    {
        [STAThread]
        static void Main(String[] args)
        {
#if DEBUG
            GameSystem.IsDebug = true;
            GameSystem.DoUnitTest = false;
#else
            GameSystem.DoUnitTest = false;
#endif

            GameSystem.Initialize(1280, 760, "TriEngine.Win Test");
            GameSystem.LoadResources(typeof(Program).Assembly);
            GameSystem.RunGameLoop(new MainGameLoop());
            GameSystem.Run();
            GameSystem.Dispose();
        }
    }
}
