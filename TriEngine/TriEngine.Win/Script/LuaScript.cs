﻿using System;
using NLua;
using Tri.Win;
using TriEngine.Win.GameLoop;

namespace TriEngine.Win.Script
{
    public enum LuaCoroutineStatus
    {
        WaitForEnter,
        WaitForFrame,
    };
    public class LuaScript
    {
        public static String CommonScript = @"
import('OpenTK')
import('Tri.Win')
import('TriEngine.Win')
import('TriEngine.Win.GameObject')
import('TriEngine.Win.Script')

function WaitForEnter()
    coroutine.yield(LuaCoroutineStatus.WaitForEnter)
end
function WaitForFrame(frameCount)
    local cnt
    for cnt = 1, frameCount do
        coroutine.yield(LuaCoroutineStatus.WaitForFrame)
    end 
end

function WriteLog(log)
    LuaScript.WriteLog(log)
end

function CallScript(scriptName)
    LuaScript.CallScript(scriptName)
end
function Exit()
    LuaScript.Exit()
end
";
        
        public static void WriteLog(String log)
        {
            GameSystem.LogSystem.WriteLine(log);
        }
        public static void CallScript(String str)
        {
            MainGameLoop currentLoop = GameSystem.CurrentGameLoop as MainGameLoop;
            if (currentLoop.LoadedList.Contains(str) == false)
            {
                currentLoop.LoadedList.Add(str);
                currentLoop.LuaScript.DoFile(String.Format("Resources/Script/{0}", str));
            }
        }
        public static void Exit()
        {
            GameSystem.Exit();
        }
    }
}
