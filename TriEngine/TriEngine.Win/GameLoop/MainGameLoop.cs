﻿using System;
using System.Collections.Generic;
using NLua;
using OpenTK;
using OpenTK.Input;
using Tri;
using Tri.Win;
using Tri.Win.GameLoop;
using Tri.Win.Rendering.Camera;
using Tri.Win.Sound;
using Tri.Win.SystemComponent;
using TriEngine.Win.GameObject;
using TriEngine.Win.Script;

namespace TriEngine.Win.GameLoop
{
    public class MainGameLoop : GameLoopBase
    {
        public Lua LuaScript;
        public List<String> LoadedList;

        private LuaCoroutineStatus lastStatus;
        static SoundContext sc;

        private OrthographicCamera cam;

        public MainGameLoop()
            : base("_MainGameLoop")
        {
        }

        public static async void LoadResource()
        {
            sc = await SoundContext.CreateAsync("Resources/Sound/ss.wav");
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();
            cam = new OrthographicCamera(GameSystem.MainForm.Viewport, 0.5f * GameSystem.MainForm.Viewport);
            sc.Play();

            LoadedList = new List<String>();
            LuaScript = new Lua();
            LuaScript.LoadCLRPackage();

            LuaScript.DoString(Script.LuaScript.CommonScript);
            LuaScript.DoString("return SpriteObject(150, 150, 100, 100, 'Image/test.png')");

            Script.LuaScript.CallScript("MainScript.lua");
            lastStatus = LuaScript.DoString<bool, LuaCoroutineStatus>(@"
__sys__co = coroutine.create(Main)
return coroutine.resume(__sys__co)
").Item2;
        }
        public override void Update(Second deltaTime)
        {
            foreach (SpriteObject obj in ObjectManager.SpriteObjectList)
                obj.Update(deltaTime);
            foreach (TextObject obj in ObjectManager.TextObjectList)
                obj.Update(deltaTime);
            
            // Need Refactoring
            if (InputHelper.IsPressed(Key.Enter) && lastStatus == LuaCoroutineStatus.WaitForEnter)
                lastStatus = LuaScript.DoString<bool, LuaCoroutineStatus>(@"
if coroutine.status(__sys__co) == 'suspended' then               
    return coroutine.resume(__sys__co)
end").Item2;
            else if (lastStatus == LuaCoroutineStatus.WaitForFrame)
                lastStatus = LuaScript.DoString<bool, LuaCoroutineStatus>(@"
if coroutine.status(__sys__co) == 'suspended' then               
    return coroutine.resume(__sys__co)
end").Item2;
        }
        public override void Render(Second deltaTime)
        {
            foreach (SpriteObject obj in ObjectManager.SpriteObjectList)
                obj.Render(ref cam.Matrix);
            foreach (TextObject obj in ObjectManager.TextObjectList)
                obj.Render(ref cam.Matrix);
        }
        public override void Dispose()
        {
            base.Dispose();
            LuaScript.Dispose();
        }
        protected override void OnStop()
        {
            base.OnStop();
        }
    }
}
