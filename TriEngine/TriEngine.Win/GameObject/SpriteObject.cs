﻿using OpenTK;
using System;
using System.IO;
using Tri;
using Tri.Win;
using Tri.Win.Rendering;
using Tri.Win.UI;

namespace TriEngine.Win.GameObject
{
    /// <summary>
    /// Wrapped class of Tri.Win.UI.Sprite
    /// </summary>
    public class SpriteObject : IDisposable
    {
        private Sprite sp;
        private Texture tex;

        #region .ctor
        public SpriteObject(float x, float y, float width, float height, String fileName)
        {
            ObjectManager.SpriteObjectList.Add(this);

            tex = Texture.CreateFromBitmapPath(Path.Combine("Resources", fileName));
            sp = new Sprite()
            {
                Position = new Vector2(x, y),
                Size = new Vector2(width, height),
                Texture = tex,
            };
            sp.IsVisible = false;
        }
        #endregion

        #region Wrapper for Lua Script
        public void Show()
        {
            sp.IsVisible = true;
        }
        public void Hide()
        {
            sp.IsVisible = false;
        }

        public void SetPosition(float x, float y)
        {
            sp.Position = new Vector2(x, y);
        }
        public Object[] GetPosition()
        {
            return new Object[]{ sp.Position.X, sp.Position.Y };
        }
        public void SetAngle(float degree)
        {
            sp.Angle = (Degree)degree;
        }
        public float GetAngle()
        {
            return (float)sp.Angle;
        }
        public void SetAnchor(float x, float y)
        {
            sp.Anchor = new Vector2(x, y);
        }
        public Object[] GetAnchor()
        {
            return new Object[]{ sp.Anchor.X, sp.Anchor.Y };
        }
        #endregion

        #region Update/Render
        public void Update(Second deltaTime)
        {
        }
        public void Render(ref Matrix4 mat)
        {
            sp.Render(ref mat);
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            ObjectManager.SpriteObjectList.Remove(this);
            if (tex != null)        
                tex.Dispose();
        }
        #endregion
        #region override (Object)
        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }
        public override String ToString()
        {
            return String.Format("SpriteObject: {0}", GetHashCode());
        }
        public override Boolean Equals(Object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }
        #endregion
    }
}
