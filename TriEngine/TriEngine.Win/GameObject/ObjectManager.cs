﻿using System.Collections.Generic;

namespace TriEngine.Win.GameObject
{
    public static class ObjectManager
    {
        static ObjectManager()
        {
            SpriteObjectList = new List<SpriteObject>();
            TextObjectList = new List<TextObject>();
        }

        public static List<SpriteObject> SpriteObjectList;
        public static List<TextObject> TextObjectList;
    }
}
