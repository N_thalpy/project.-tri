﻿using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;
using System.IO;
using Tri;
using Tri.Win;
using Tri.Win.Rendering;
using Tri.Win.UI;
using Tri.Win.UI.BitmapFont;

namespace TriEngine.Win.GameObject
{
    /// <summary>
    /// Wrapped class of Tri.Win.UI.Sprite
    /// </summary>
    public class TextObject : IDisposable
    {
        private BitmapLabel lb;

        #region .ctor
        public TextObject(float x, float y, String fontName, float size)
        {
            ObjectManager.TextObjectList.Add(this);

            lb = new BitmapLabel()
            {
                Position = new Vector2(x, y),
                Text = String.Empty,
                BitmapFont = BitmapFont.Create("Resources/BitmapFont/arial.fnt"),
                Color = Color4.Black,
            };
            lb.IsVisible = false;
        }
        #endregion

        #region Wrapper for Lua Script
        public void Show()
        {
            lb.IsVisible = true;
        }
        public void Hide()
        {
            lb.IsVisible = false;
        }

        public void SetPosition(float x, float y)
        {
            lb.Position = new Vector2(x, y);
        }
        public Object[] GetPosition()
        {
            return new Object[]{ lb.Position.X, lb.Position.Y };
        }
        public void SetAngle(float degree)
        {
            lb.Angle = (Degree)degree;
        }
        public float GetAngle()
        {
            return (float)lb.Angle;
        }
        public void SetAnchor(float x, float y)
        {
            lb.Anchor = new Vector2(x, y);
        }
        public Object[] GetAnchor()
        {
            return new Object[]{ lb.Anchor.X, lb.Anchor.Y };
        }
        public void SetText(String str)
        {
            lb.Text = str;
        }
        public String GetText()
        {
            return lb.Text;
        }
        public void SetFont(String fontName, float size)
        {
            lb.BitmapFont.Load(fontName);
        }
        public void SetColor(float r, float g, float b, float a)
        {
            lb.Color = new Color4(r, g, b, a);
        }
        public Object[] GetColor()
        {
            return new Object[]{ lb.Color.R, lb.Color.G, lb.Color.B, lb.Color.A };
        }

        public void ChangeRotation(float degree)
        {
            lb.Angle = (Degree)degree;
        }
        public void ChangePosition(float x, float y)
        {
            lb.Position = new Vector2(x, y);
        }
        #endregion

        #region Update/Render
        public void Update(Second deltaTime)
        {
        }
        public void Render(ref Matrix4 mat)
        {
            lb.Render(ref mat);
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            ObjectManager.TextObjectList.Remove(this);
            lb.Dispose();
        }
        #endregion
        #region override (Object)
        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }
        public override String ToString()
        {
            return String.Format("TextObject: {0}", GetHashCode());
        }
        public override Boolean Equals(Object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }
        #endregion
    }
}
